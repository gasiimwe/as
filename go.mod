module gitlab.com/gasiimwe/as

go 1.15

require (
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/consul/api v1.8.1
	github.com/onsi/ginkgo v1.12.1
	github.com/onsi/gomega v1.10.5
	github.com/prometheus/client_golang v1.9.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/yaml.v2 v2.3.0
	gorm.io/driver/postgres v1.0.8
	gorm.io/driver/sqlite v1.1.4 // indirect
	gorm.io/gorm v1.20.12
)
