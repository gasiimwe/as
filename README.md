# UWA AUTHORIZATION-SERVICE

**LOGIN**
  Endpoint     : `/api/v1/authorization-service/login`
  Protocol     : `POST`
  Payload      : `{"username": "", "password": ""}`
  Content Type : `application/json`
  Response     : `{"token": "", "expires_at": ""}`
  Code         : 200

**SESSION REFRESH**
  Endpoint     : `/api/v1/authorization-service/refresh`
  Protocol     : `GET`
  Header       : `Authorization : Bearer {AUTH-TOKEN}`
  Response     : `{"token": "", "expires_at": ""}`
  Code         : 200

**LOGOUT**
  Endpoint     : `/api/v1/authorization-service/logout`
  Protocol     : `PUT`
  Header       : `Authorization : Bearer {AUTH-TOKEN}`
  Code         : 204

**USER DETAILS**
  Endpoint     : `/api/v1/authorization-service/details`
  Protocol     : `GET`
  Header       : `Authorization : Bearer {AUTH-TOKEN}`
  Code         : 200
  Response     :   
  ```json
 {
  "first_name":"",
  "last_name":"", 
  "gender": "", 
  "email": "",
  "phone_number:"",
  "username": "",
  "roles": [],
  "permissions": [],
  "uid":"", 
  "photo_id":"",
  "photo_url:"",		    
 }
```


  
###NB:
 All failure responses are have a json body with the following details 
  `{"error": "", "detail": "", "occurred_at": ""}` 

  `error`      : The type of error that occurred
  `detail`     : Detailed message explaining the error itself 
  `occurred_at`: Timestamp for the time the error occurred

The end points details are subject to change as will be noted with the documentation

