# do golang stuff
FROM golang:1.13-alpine as build_base

# Install some dependencies needed to build the project
RUN apk add bash ca-certificates git gcc g++ libc-dev

RUN mkdir -p /go/src/gitlab.com/mtwa/authorization-service/
COPY . /go/src/gitlab.com/mtwa/authorization-service/
WORKDIR /go/src/gitlab.com/mtwa/authorization-service/

# Force the go compiler to use modules
ENV GO111MODULE=on

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

#This is the ‘magic’ step that will download all the dependencies that are specified in 
# the go.mod and go.sum file.
# Because of how the layer caching system works in Docker, the  go mod download 
# command will _ only_ be re-run when the go.mod or go.sum file change 
# (or when we add another docker instruction this line)
RUN go mod download
RUN go get -u github.com/go-bindata/go-bindata/...

# This image builds the authentication server
FROM build_base AS server_builder
# Here we copy the rest of the source code
COPY . .

# build
RUN chmod +x ./bundle-resources.sh
RUN ./bundle-resources.sh
# RUN make build

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o authorization-service

FROM alpine:3.7 AS app
RUN apk add --update bash
RUN apk --update upgrade && \
     apk add curl ca-certificates && \
     update-ca-certificates && \
     rm -rf /var/cache/apk/*

COPY --from=server_builder /go/src/gitlab.com/mtwa/authorization-service/authorization-service ./app/
COPY --from=server_builder /go/src/gitlab.com/mtwa/authorization-service/run_prod.sh ./app/

EXPOSE 4010

ENTRYPOINT ["./app/run_prod.sh"]
