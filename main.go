package main

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/gasiimwe/as/app"
)

func main() {
	application := app.Application{}
	application.Start()
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
	application.Stop()

}
