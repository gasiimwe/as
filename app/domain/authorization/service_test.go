package authorization_test

// import (
// 	"fmt"
// 	"testing"

// 	"database/sql"

// 	. "github.com/onsi/ginkgo"
// 	. "github.com/onsi/gomega"
// 	uuid "github.com/satori/go.uuid"
// 	"gitlab.com/uwa/authorization-service/app/domain/authorization"

// 	"gorm.io/driver/sqlite"
// 	"gorm.io/gorm"
// )

// func TestAuthorizationService(t *testing.T) {
// 	RegisterFailHandler(Fail)
// 	RunSpecs(t, "Authorization Service Test Suite")
// }

// type testDataSource struct {
// 	db *gorm.DB
// }

// func (tds *testDataSource) Connection() *gorm.DB {
// 	if tds.db == nil {
// 		var err error
// 		tds.db, err = gorm.Open(
// 			sqlite.Open("file::memory:?mode=memory&cache=shared"),
// 			&gorm.Config{})
// 		if err != nil {
// 			panic(err)
// 		}
// 	}
// 	return tds.db
// }

// func (tds *testDataSource) Close() error {
// 	if tds.db != nil {
// 		return tds.Close()
// 	}
// 	return nil
// }

// func dataSourceProvider() database.DataSource {
// 	return &testDataSource{}
// }

// func migrate(dataSource database.DataSource) {
// 	rule0 := `CREATE TRIGGER IF NOT EXISTS update_permission_idx AFTER INSERT ON permission_tb
// 		   BEGIN
// 			UPDATE permission_tb SET idx=id WHERE id=NEW.id;
// 		   END;`

// 	rule1 := `CREATE TRIGGER update_role_idx AFTER INSERT ON role_tb
// 			  BEGIN
// 			   UPDATE role_tb SET idx=id WHERE id=NEW.id;
// 			  END;`
// 	rule2 := `CREATE TRIGGER update_user_idx AFTER INSERT ON user_tb
// 			  BEGIN
// 			   UPDATE user_tb SET idx=id WHERE id=NEW.id;
// 			  END;`

// 	dataSource.Connection().AutoMigrate(&Permission{}, &Role{}, &User{})
// 	dataSource.Connection().Exec(rule0)
// 	dataSource.Connection().Exec(rule1)
// 	dataSource.Connection().Exec(rule2)
// }

// func addUser(dataSource database.DataSource, user User, Roles []string) error {
// 	tx := dataSource.Connection().Begin()
// 	user.Roles = nil
// 	if err := tx.Where("name IN (?)", Roles).Find(&user.Roles).Error; err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	if len(user.Roles) == 0 {
// 		tx.Rollback()
// 		return errors.New(NO_ROLES_FOUND)
// 	}
// 	if err := tx.Omit("idx").Create(user).Error; err != nil {
// 		if strings.Contains(strings.ToLower(err.Error()), "unique") {
// 			tx.Rollback()
// 			return errors.New(MATCHING_USER_EXISTS)
// 		}
// 		return err
// 	}
// 	if err := tx.Model(user).Association("Roles").Error; err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	if err := tx.Model(user).Association("Roles").Append(user.Roles); err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	return tx.Commit().Error
// }

// func addRole(dataSource database.DataSource, RoleName string, addedPermissions []string) {
// 	added := make([]Permission, 0)
// 	for _, permission := range addedPermissions {
// 		added = append(added, Permission{Name: permission})
// 	}
// 	addPermissions(dataSource, added)
// 	addRoleContent(dataSource, Role{Name: RoleName, Permissions: added})
// }

// func addRoleContent(dataSource database.DataSource, role Role) error {
// 	tx := dataSource.Connection().Begin()
// 	if err := tx.Omit("idx").Create(Role).Error; err != nil {
// 		tx.Rollback()
// 		if strings.Contains(strings.ToLower(err.Error()), "unique") {
// 			return errors.New(NON_UNIQUE_ROLE)
// 		}
// 		return err
// 	}
// 	if err := tx.Model(Role).Association("Permissions").Error; err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	if err := tx.Model(Role).Association("Permissions").Append(Role.Permissions); err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	return tx.Commit().Error
// }

// func addPermissions(dataSource database.DataSource, permissions []Permission) error {
// 	valueEntries := []string{}
// 	valueArguments := []interface{}{}

// 	for _, permission := range permissions {
// 		valueEntries = append(valueEntries, "(?)")
// 		valueArguments = append(valueArguments, permission.Name)
// 	}

// 	var sql = `INSERT INTO permission_tb(name) VALUES %s`

// 	sql = fmt.Sprintf(sql, strings.Join(valueEntries, ","))
// 	tx := dataSource.Connection().Begin()
// 	if err := tx.Exec(sql, valueArguments...).Error; err != nil {
// 		tx.Rollback()
// 		if strings.Contains(strings.ToLower(err.Error()), "unique") {
// 			return errors.New(DUPLICATE_ERROR)
// 		}
// 		return err
// 	}
// 	if err := tx.Commit().Error; err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	return nil
// }
