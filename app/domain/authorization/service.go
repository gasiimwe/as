package authorization

const (
	CREDENTIALS_DO_NOT_MATCH = "Credentials are not a match for any account"
	ACCOUNT_NOT_ACTIVATED    = "Account Not Activated"
)

// AuthorizationService :
type AuthorizationService interface {
	RequestSession(credentials *Credentials) (TokenValue, error)
	FetchUserDetails(token string) (UserDetails, error)
	RenewSession(oldToken string) (TokenValue, error)
	EndSession(token string) error
}
