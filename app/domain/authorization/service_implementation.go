package authorization

import (
	"errors"
	"time"

	"fmt"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/as/app/infrastructure/configuration"
	"gitlab.com/gasiimwe/as/app/infrastructure/security"
	jwt "gopkg.in/square/go-jose.v2/jwt"
)

type JWEAuthorizationService struct {
	repository      AuthorizationRepository
	passwordHandler security.PasswordHandler
	tokenHandler    security.TokenHandler
	configs         configuration.Configuration
}

func NewAuthorizationService(
	repository AuthorizationRepository,
	passwordHandler security.PasswordHandler,
	tokenHandler security.TokenHandler,
	parameters configuration.Configuration) AuthorizationService {
	return &JWEAuthorizationService{
		repository:      repository,
		passwordHandler: passwordHandler,
		tokenHandler:    tokenHandler,
		configs:         parameters,
	}
}

func (jas *JWEAuthorizationService) RequestSession(credentials *Credentials) (TokenValue, error) {
	user, err := jas.repository.FetchUserByUsername(credentials.Username)
	if err != nil {
		return TokenValue{}, nil
	}
	if !user.Activated {
		return TokenValue{}, errors.New(ACCOUNT_NOT_ACTIVATED)
	}
	match, err := jas.passwordHandler.CompareHashToPassword(credentials.Password, user.Password)
	if err != nil {
		return TokenValue{}, nil
	}
	if !match {
		return TokenValue{}, errors.New(CREDENTIALS_DO_NOT_MATCH)
	}
	location, _ := time.LoadLocation("UTC")
	currentTime := time.Now().In(location)
	expiresAt := time.Now().Add(time.Minute * jas.configs.SessionDuration).In(location)
	token, err := jas.createToken(user, currentTime, expiresAt)
	if err != nil {
		return TokenValue{}, err
	}
	if err := jas.repository.AddSession(token, user.UID, expiresAt); err != nil {
		return TokenValue{}, err
	}
	return TokenValue{Token: token, ExpiresAt: expiresAt.Format(time.RFC3339)}, nil
}

func (jas *JWEAuthorizationService) FetchUserDetails(token string) (UserDetails, error) {
	claims, err := jas.tokenHandler.ParseToken(token)
	if err != nil {
		return UserDetails{}, err
	}
	if claims.Expiry.Time().Before(time.Now()) {
		if err := jas.repository.RemoveSession(claims.ID); err != nil {
			return UserDetails{}, err
		}
		return UserDetails{}, errors.New(SESSION_ALREADY_EXPIRED)
	}
	user, err := jas.repository.FetchByUID(uuid.FromStringOrNil(claims.ID))
	if err != nil {
		return UserDetails{}, err
	}
	return jas.details(user), nil
}

func (jas *JWEAuthorizationService) RenewSession(oldToken string) (TokenValue, error) {
	claims, err := jas.tokenHandler.ParseToken(oldToken)
	if err != nil {
		return TokenValue{}, err
	}
	if claims.Expiry.Time().Before(time.Now()) {
		if err := jas.repository.RemoveSession(claims.ID); err != nil {
			return TokenValue{}, err
		}
		return TokenValue{}, errors.New(SESSION_ALREADY_EXPIRED)
	}
	user, err := jas.repository.FetchByUID(uuid.FromStringOrNil(claims.ID))
	if err != nil {
		return TokenValue{}, err
	}
	location, _ := time.LoadLocation("UTC")
	currentTime := time.Now().In(location)
	expiresAt := time.Now().Add(time.Minute * jas.configs.SessionDuration).In(location)
	token, err := jas.createToken(user, currentTime, expiresAt)
	if err != nil {
		return TokenValue{}, err
	}
	if err := jas.repository.AddSession(token, user.UID, expiresAt); err != nil {
		return TokenValue{}, err
	}
	return TokenValue{Token: token, ExpiresAt: expiresAt.Format(time.RFC3339)}, nil
}

func (jas *JWEAuthorizationService) EndSession(token string) error {
	claims, err := jas.tokenHandler.ParseToken(token)
	if err != nil {
		return err
	}
	return jas.repository.RemoveSession(claims.ID)
}

func (jas *JWEAuthorizationService) createToken(
	user *User,
	currentTime time.Time,
	expiryTime time.Time) (string, error) {
	roles := make([]string, 0)
	permissions := make([]string, 0)
	for _, v := range user.Roles {
		roles = append(roles, v.Name)
		for _, p := range v.Permissions {
			permissions = append(permissions, p.Name)
		}
	}

	claims := &security.Claims{
		Issuer:       jas.configs.Issuer,
		ID:           user.UID.String(),
		Subject:      user.UID.String(),
		Audience:     jwt.Audience{user.Audience},
		Expiry:       jwt.NewNumericDate(expiryTime),
		IssuedAt:     jwt.NewNumericDate(currentTime),
		Roles:        roles,
		Permissions:  permissions,
		Organization: user.Organization,
	}
	return jas.tokenHandler.CreateToken(claims)
}

func (jas *JWEAuthorizationService) details(user *User) UserDetails {
	roles := make([]string, 0)
	permissions := make([]string, 0)
	for _, v := range user.Roles {
		roles = append(roles, v.Name)
		for _, p := range v.Permissions {
			permissions = append(permissions, p.Name)
		}
	}
	return UserDetails{
		Uid:         user.UID.String(),
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Gender:      user.Gender,
		Email:       user.Email,
		Phone:       user.Phone,
		PhotoId:     user.PhotoID,
		PhotoURL:    fmt.Sprintf("%s/%s", jas.configs.BucketBaseURL, user.PhotoID),
		Roles:       roles,
		Permissions: permissions,
		About:       user.About,
	}
}
