package authorization

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gasiimwe/as/app/infrastructure/server/controllers"
	"gitlab.com/gasiimwe/as/app/infrastructure/server/registry"
)

const BASE_PATH = "/api/v1/authorization-service"

type AuthorizationController struct {
	*controllers.ControllerBase
	service AuthorizationService
}

func NewController(service AuthorizationService) *AuthorizationController {
	return &AuthorizationController{service: service}
}

func (ac *AuthorizationController) Name() string {
	return "authorization-controller"
}

func (ac *AuthorizationController) Initialize(RouteRegistry registry.RouterRegistry) {
	RouteRegistry.Add(
		BASE_PATH+"/login",
		false,
		"POST",
		ac.login,
	)
	RouteRegistry.Add(
		BASE_PATH+"/refresh",
		true,
		"GET",
		ac.refresh,
	)
	RouteRegistry.Add(
		BASE_PATH+"/details",
		true,
		"GET",
		ac.details,
	)
	RouteRegistry.Add(
		BASE_PATH+"/logout",
		true,
		"PUT",
		ac.logout,
	)
}

func (ac *AuthorizationController) login(w http.ResponseWriter, r *http.Request) {
	credentials := &Credentials{}
	if err := json.NewDecoder(r.Body).Decode(credentials); err != nil {
		ac.BadRequest(w, err)
	}
	token, err := ac.service.RequestSession(credentials)
	if err != nil {
		ac.ServiceFailure(w, err)
	} else {
		ac.OK(w, token)
	}
}

func (ac *AuthorizationController) refresh(w http.ResponseWriter, r *http.Request) {
	oldToken := ac.GetAccessToken(r)
	updatedToken, err := ac.service.RenewSession(oldToken)
	if err != nil {
		ac.ServiceFailure(w, err)
	} else {
		ac.OK(w, updatedToken)
	}
}

func (ac *AuthorizationController) details(w http.ResponseWriter, r *http.Request) {
	token := ac.GetAccessToken(r)
	details, err := ac.service.FetchUserDetails(token)
	if err != nil {
		ac.ServiceFailure(w, err)
	} else {
		ac.OK(w, details)
	}
}

func (ac *AuthorizationController) logout(w http.ResponseWriter, r *http.Request) {
	token := ac.GetAccessToken(r)
	if err := ac.service.EndSession(token); err != nil {
		ac.ServiceFailure(w, err)
	} else {
		ac.OKNoResponse(w)
	}
}
