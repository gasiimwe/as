package authorization

import (
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/gasiimwe/as/app/infrastructure/database"
)

type SqlAuthorizationRepository struct {
	dataSource database.DataSource
}

func NewAuthorizationRepository(dataSource database.DataSource) AuthorizationRepository {
	instance := &SqlAuthorizationRepository{
		dataSource: dataSource,
	}
	return instance
}

func (st *SqlAuthorizationRepository) FetchUserByUsername(username string) (*User, error) {
	user := &User{}
	connection := st.dataSource.Connection()
	if connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Where("username = ?", username).
		First(user).RowsAffected == 0 {
		return nil, errors.New(UNKNOWN_USER)
	}
	return user, nil
}

func (st *SqlAuthorizationRepository) FetchByUID(uid uuid.UUID) (*User, error) {
	user := &User{}
	connection := st.dataSource.Connection()
	if connection.
		Preload("Roles").
		Preload("Roles.Permissions").
		Where("uid = ?", uid).
		First(user).RowsAffected == 0 {
		return nil, errors.New(UNKNOWN_USER)
	}
	return user, nil
}

func (st *SqlAuthorizationRepository) AddSession(token string, uid uuid.UUID, expiresAt time.Time) error {
	tx := st.dataSource.Connection().Begin()
	if err := tx.Raw("DELETE FROM session_tb WHERE user_id = "+
		"(SELECT id FROM user_tb WHERE uid = ?)", uid).Error; err != nil {
		tx.Rollback()
		return err
	}
	sql := "INSERT INTO session_tb(user_id, token, created_at) " +
		"VALUES(SELECT id FROM user_tb WHERE uid = ?), ?, ?)"
	if err := tx.Raw(sql, uid, token, expiresAt).Error; err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

func (st *SqlAuthorizationRepository) RemoveSession(uid string) error {
	tx := st.dataSource.Connection().Begin()
	if err := tx.Unscoped().Raw(
		"DELETE FROM session_tb "+
			"WHERE user_id = (SELECT id FROM user_tb WHERE uid = ?)", uid).Error; err != nil {
		tx.Rollback()
		return errors.New(NO_SESSION_FOUND)
	}
	return tx.Commit().Error
}
