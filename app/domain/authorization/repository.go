package authorization

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

const (
	NO_SESSION_FOUND        = "NO MATCHING SESSION FOUND"
	SESSION_ALREADY_EXPIRED = "SESSION ALREADY EXPIRED"
	MATCHING_USER_EXISTS    = "MATCHING USER ALREADY EXISTS"
	UNKNOWN_USER            = "NO MATCHING USER FOUND"
)

type AuthorizationRepository interface {
	FetchByUID(uid uuid.UUID) (*User, error)
	FetchUserByUsername(username string) (*User, error)
	AddSession(token string, uid uuid.UUID, expiresAt time.Time) error
	RemoveSession(uid string) error
}
