package authorization

// TokenValue :
type TokenValue struct {
	Token     string `json:"token"`
	ExpiresAt string `json:"expires_at"`
}

// Credentials :
type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// UserDetails :
type UserDetails struct {
	Uid         string   `json:"uid"`
	FirstName   string   `json:"first_name"`
	LastName    string   `json:"last_name"`
	Gender      string   `json:"gender"`
	Email       string   `json:"email"`
	Phone       string   `json:"phone_number"`
	PhotoId     string   `json:"photo_id,omitempty"`
	PhotoURL    string   `json:"photo_url,omitempty"`
	Roles       []string `json:"roles,omitempty"`
	Permissions []string `json:"permissions"`
	About       string   `json:"about"`
}
