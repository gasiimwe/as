package security

type TokenHandler interface {
	CreateToken(Claims *Claims) (string, error)
	ParseToken(Token string) (*Claims, error)
}
