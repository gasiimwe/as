package security

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"

	log "github.com/sirupsen/logrus"
	"gitlab.com/gasiimwe/as/app/infrastructure/logging"
	jose "gopkg.in/square/go-jose.v2"
	jwt "gopkg.in/square/go-jose.v2/jwt"
)

const (
	RS_256                 = "RS256"
	RS_384                 = "RS384"
	RS_512                 = "RS512"
	NO_SIGNING_KEY_PRESENT = "No Signing Key Present"
	MALFORMED_TOKEN_ERROR  = "Session Token is malformed"
	TOKEN_EXPIRED          = "Session has expired"
	INVALID_TOKEN_ERROR    = "Token is not valid or possibly not signed from this service"
)

var (
	ErrKeyMustBePEMEncoded = errors.New("Invalid Key: Key must be PEM encoded PKCS1 or PKCS8 private key")
	ErrNotRSAPrivateKey    = errors.New("Key is not a valid RSA private key")
	ErrNotRSAPublicKey     = errors.New("Key is not a valid RSA public key")
)

type JWERSATokenHandler struct {
	publicKey  *rsa.PublicKey
	privateKey *rsa.PrivateKey
	log        *log.Logger
}

func New(privateKeyBytes, publicKeyBytes []byte) TokenHandler {
	instance := &JWERSATokenHandler{
		log: logging.GetInstance(),
	}
	instance.init(privateKeyBytes, publicKeyBytes)
	return instance
}

func (rs *JWERSATokenHandler) init(privateKeyBytes, publicKeyBytes []byte) {
	var err error
	rs.privateKey, err = ParseRSAPrivateKeyFromPEM(privateKeyBytes)
	if err != nil {
		panic(err)
	}
	rs.publicKey, err = ParseRSAPublicKeyFromPEM(publicKeyBytes)
	if err != nil {
		panic(err)
	}
	rs.log.Println("Completed loading RSA Private Key")
}

func (rs *JWERSATokenHandler) CreateToken(Claims *Claims) (string, error) {
	if rs.publicKey == nil {
		return "", errors.New(NO_SIGNING_KEY_PRESENT)
	}
	enc, err := jose.NewEncrypter(
		jose.A128GCM,
		jose.Recipient{Algorithm: jose.RSA_OAEP, Key: rs.publicKey},
		(&jose.EncrypterOptions{}).WithType("JWT"),
	)
	if err != nil {
		return "", err
	}
	raw, err := jwt.Encrypted(enc).Claims(Claims).CompactSerialize()
	if err != nil {
		return "", err
	}
	return raw, nil
}

func (rs *JWERSATokenHandler) ParseToken(Token string) (*Claims, error) {
	claims := &Claims{}
	tok, err := jwt.ParseEncrypted(Token)
	if err != nil {
		log.Printf(err.Error())
		return claims, errors.New(MALFORMED_TOKEN_ERROR)
	}
	if err := tok.Claims(rs.privateKey, &claims); err != nil {
		log.Printf(err.Error())
		return claims, errors.New(MALFORMED_TOKEN_ERROR)
	}
	return claims, nil
}

// Parse PEM encoded PKCS1 or PKCS8 private key
func ParseRSAPrivateKeyFromPEM(key []byte) (*rsa.PrivateKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, ErrKeyMustBePEMEncoded
	}

	var parsedKey interface{}
	if parsedKey, err = x509.ParsePKCS1PrivateKey(block.Bytes); err != nil {
		if parsedKey, err = x509.ParsePKCS8PrivateKey(block.Bytes); err != nil {
			return nil, err
		}
	}

	var pkey *rsa.PrivateKey
	var ok bool
	if pkey, ok = parsedKey.(*rsa.PrivateKey); !ok {
		return nil, ErrNotRSAPrivateKey
	}
	return pkey, nil
}

// Parse PEM encoded PKCS1 or PKCS8 private key protected with password
func ParseRSAPrivateKeyFromPEMWithPassword(key []byte, password string) (*rsa.PrivateKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, ErrKeyMustBePEMEncoded
	}

	var parsedKey interface{}

	var blockDecrypted []byte
	if blockDecrypted, err = x509.DecryptPEMBlock(block, []byte(password)); err != nil {
		return nil, err
	}

	if parsedKey, err = x509.ParsePKCS1PrivateKey(blockDecrypted); err != nil {
		if parsedKey, err = x509.ParsePKCS8PrivateKey(blockDecrypted); err != nil {
			return nil, err
		}
	}

	var pkey *rsa.PrivateKey
	var ok bool
	if pkey, ok = parsedKey.(*rsa.PrivateKey); !ok {
		return nil, ErrNotRSAPrivateKey
	}

	return pkey, nil
}

// Parse PEM encoded PKCS1 or PKCS8 public key
func ParseRSAPublicKeyFromPEM(key []byte) (*rsa.PublicKey, error) {
	var err error

	// Parse PEM block
	var block *pem.Block
	if block, _ = pem.Decode(key); block == nil {
		return nil, ErrKeyMustBePEMEncoded
	}

	// Parse the key
	var parsedKey interface{}
	if parsedKey, err = x509.ParsePKIXPublicKey(block.Bytes); err != nil {
		if cert, err := x509.ParseCertificate(block.Bytes); err == nil {
			parsedKey = cert.PublicKey
		} else {
			return nil, err
		}
	}

	var pkey *rsa.PublicKey
	var ok bool
	if pkey, ok = parsedKey.(*rsa.PublicKey); !ok {
		return nil, ErrNotRSAPublicKey
	}

	return pkey, nil
}
