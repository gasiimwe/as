package discovery

type Service interface {
	FetchConfigurationsData(string) ([]byte, error)
	// Get a Service from consul
	Service(string, string) ([]string, error)
	// Register a service with local agent
	Register(name string, port int, tags []string) error
	// Deregister a service with local agent
	DeRegister(string) error
}
