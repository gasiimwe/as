package controllers

import (
	"gitlab.com/gasiimwe/as/app/infrastructure/logging"
	"gitlab.com/gasiimwe/as/app/infrastructure/server/registry"
)

type ControllerRegistry struct {
	routeRegistry registry.RouterRegistry
	controllers   map[string]Controller
}

func New(routeRegistry registry.RouterRegistry) *ControllerRegistry {
	return &ControllerRegistry{
		routeRegistry: routeRegistry,
		controllers:   make(map[string]Controller),
	}
}

func (cr *ControllerRegistry) AddController(controller Controller) {
	cr.controllers[controller.Name()] = controller
}

func (cr *ControllerRegistry) InitializeControllers() {
	LOG := logging.GetInstance()
	for name, controller := range cr.controllers {
		LOG.Infof("CONTROLLER -> %s", name)
		controller.Initialize(cr.routeRegistry)
	}
}
