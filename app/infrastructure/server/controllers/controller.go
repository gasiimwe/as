package controllers

import (
	"gitlab.com/gasiimwe/as/app/infrastructure/server/registry"
)

type Controller interface {
	Name() string
	Initialize(RouteRegistry registry.RouterRegistry)
}
