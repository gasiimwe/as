package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	h "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/gasiimwe/as/app/infrastructure/logging"
	"gitlab.com/gasiimwe/as/app/infrastructure/metrics"
	"gitlab.com/gasiimwe/as/app/infrastructure/security"
	"gitlab.com/gasiimwe/as/app/infrastructure/server/controllers"
	"gitlab.com/gasiimwe/as/app/infrastructure/server/registry"
)

type ServerCore struct {
	tokenHandler                   security.TokenHandler
	routerRegistry                 registry.RouterRegistry
	router                         *mux.Router
	server                         *http.Server
	controllerRegistry             *controllers.ControllerRegistry
	host                           string
	port                           int
	startupCallback                func() bool
	shutdownCallback               func() bool
	healthCheckCallback            func() (interface{}, error)
	middlewareRegistrationCallback func(router *mux.Router)
	metricsPusher                  metrics.MetricsPusher
	details                        Details
}

func New(details Details, host string, port int, tokenHandler security.TokenHandler) *ServerCore {
	instance := &ServerCore{
		details:      details,
		host:         host,
		port:         port,
		tokenHandler: tokenHandler,
	}
	instance.initialize()
	return instance
}

func (sc *ServerCore) initialize() {
	sc.router = mux.NewRouter()
	sc.routerRegistry = registry.New(sc.router, sc.tokenHandler)
	sc.controllerRegistry = controllers.New(sc.routerRegistry)
	sc.metricsPusher = metrics.New(true)
}

func (sc *ServerCore) OnMiddlewareRegistration(middlewareRegistrationCallback func(router *mux.Router)) {
	sc.middlewareRegistrationCallback = middlewareRegistrationCallback
}

// GetControllerRegistry : Get an instance of the controller registry
func (sc *ServerCore) GetControllerRegistry() *controllers.ControllerRegistry {
	return sc.controllerRegistry
}

func (sc *ServerCore) GetMetricsRegistry() *prometheus.Registry {
	return sc.metricsPusher.Registry()
}

// OnStartUp : Triggered on a start up event
func (sc *ServerCore) OnStartUp(startupCallback func() bool) {
	sc.startupCallback = startupCallback
}

// OnShutDown : Triggered on a shutdown event
func (sc *ServerCore) OnShutDown(shutdownCallback func() bool) {
	sc.shutdownCallback = shutdownCallback
}

// OnHealthCheck : Triggered on a call to the HC endpoint
func (sc *ServerCore) OnHealthCheck(healthCheckCallback func() (interface{}, error)) {
	sc.healthCheckCallback = healthCheckCallback
}

// Start Server
func (sc *ServerCore) Start() {
	sc.controllerRegistry.AddController(controllers.NewHealthChecker(sc.healthCheckCallback))
	sc.controllerRegistry.AddController(controllers.NewMetricController(sc.metricsPusher))
	if sc.startupCallback() {
		sc.middlewareRegistrationCallback(sc.router)
		sc.controllerRegistry.InitializeControllers()
		sc.startUpServer()
	}
}

func (sc *ServerCore) startUpServer() {
	sc.server = &http.Server{
		Addr: fmt.Sprintf("%s:%d", sc.host, sc.port),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler: h.CORS(h.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
			h.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}),
			h.AllowedOrigins([]string{"*"}))(sc.router),
	}
	sc.routerRegistry.Initialize()

	go func() {
		sc.logoPrint()
		LOG := logging.GetInstance()
		LOG.Infof("\n\n")
		LOG.Infof("Starting    %s \n", sc.details.Name)
		LOG.Infof("Version     %s \n", sc.details.Version)
		LOG.Infof("Environment %s \n", sc.details.Environment)
		LOG.Infof("Repository  %s \n", sc.details.Repository)
		LOG.Infof("Commit Hash %s \n", sc.details.Hash)
		LOG.Infof("Build Date  %s \n", sc.details.BuildDate)
		LOG.Infof("Build Epoch %d \n", sc.details.BuildEpoch)

		if err := sc.server.ListenAndServe(); err != nil {
			LOG.Error(err)
		}
	}()
}

func (sc *ServerCore) Stop() {
	if sc.shutdownCallback() {
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()
		sc.server.Shutdown(ctx)
		// Optionally, you could run srv.Shutdown in a goroutine and block on
		// <-ctx.Done() if your application should wait for other services
		// to finalize based on context cancellation.
		fmt.Println("shutting down")
		os.Exit(0)
	}
}

func (sc *ServerCore) logoPrint() {
	logoData, err := Asset("resources/boot.txt")
	if err != nil {
		logging.GetInstance().Error(err)
	} else {
		fmt.Print(string(logoData))
		fmt.Println()
	}
}
