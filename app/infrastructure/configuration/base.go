package configuration

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/gasiimwe/as/app/infrastructure/discovery"
	"gitlab.com/gasiimwe/as/app/infrastructure/security"
	"gitlab.com/gasiimwe/as/app/infrastructure/server"
	yaml "gopkg.in/yaml.v2"
)

type Configuration struct {
	Host                 string                         `yaml:"host"`
	Port                 int                            `yaml:"port"`
	DatabaseParameters   *DatabaseParameters            `yaml:"database"`
	EncryptionParameters *security.EncryptionParameters `yaml:"encryption"`
	SessionDuration      time.Duration                  `yaml:"session-duration"`
	Issuer               string                         `yaml:"issuer"`
	BucketBaseURL        string                         `yaml:"bucket-base"`
}

type DatabaseParameters struct {
	URI                string `yaml:"uri"`
	MaxOpenConnections int    `yaml:"max-open-connections"`
	MaxIdleConnections int    `yaml:"max-idle-connections"`
}

func ParseFromResource() Configuration {
	yamlFile, err := server.Asset("resources/settings.yml")
	config := Configuration{}
	if err != nil {
		fmt.Println("Failed to open the file")
		panic(err)
	}
	yaml.Unmarshal(yamlFile, &config)
	return config
}

func ParseFromConsul(Service discovery.Service, Name string) Configuration {
	yamlFile, err := Service.FetchConfigurationsData(Name)
	config := Configuration{}
	if err != nil {
		fmt.Println("Failed to open the file")
		panic(err)
	}
	yaml.Unmarshal(yamlFile, &config)
	return config
}

func IsConsulEnabled() bool {
	value := os.Getenv("CONSUL_ENABLED")
	if len(value) == 0 || value != "true" {
		return false
	}
	return true
}
