package app

import (
	"time"

	"gitlab.com/gasiimwe/as/app/domain/authorization"
	"gitlab.com/gasiimwe/as/app/infrastructure/configuration"
	"gitlab.com/gasiimwe/as/app/infrastructure/database"
	"gitlab.com/gasiimwe/as/app/infrastructure/discovery"
	"gitlab.com/gasiimwe/as/app/infrastructure/security"
	"gitlab.com/gasiimwe/as/app/infrastructure/server"
	"gitlab.com/gasiimwe/as/app/infrastructure/server/controllers"
)

type ServiceResources struct {
	discoveryService discovery.Service
	server           *server.ServerCore
	tokenHandler     security.TokenHandler
	parameters       configuration.Configuration
	passwordHandler  security.PasswordHandler
	dataSource       database.DataSource
	repository       authorization.AuthorizationRepository
}

func ProduceServiceResources(
	discoveryService discovery.Service,
	server *server.ServerCore,
	parameters configuration.Configuration,
	tokenHandler security.TokenHandler) *ServiceResources {
	return &ServiceResources{
		discoveryService: discoveryService,
		server:           server,
		parameters:       parameters,
		tokenHandler:     tokenHandler,
	}
}

func (sr *ServiceResources) Initialize() {
	sr.dataSource = sr.createDataSource()
	sr.setUpRepository()
	sr.setUpPasswordHandler()
	controllerRegistry := sr.server.GetControllerRegistry()
	sr.setUpAuthorizationResource(controllerRegistry)
}

func (sr *ServiceResources) setUpRepository() {
	sr.repository = authorization.NewAuthorizationRepository(sr.dataSource)
}

func (sr *ServiceResources) createDataSource() database.DataSource {
	return database.
		NewBuilder().
		EnableLogging().
		MaxConnectionLifeTime(time.Hour).
		MetricRegistry(sr.server.GetMetricsRegistry()).
		MaxIdleConnections(sr.parameters.DatabaseParameters.MaxIdleConnections).
		MaxOpenConnections(sr.parameters.DatabaseParameters.MaxOpenConnections).
		Uri(sr.parameters.DatabaseParameters.URI).
		Build()
}

func (sr *ServiceResources) setUpPasswordHandler() {
	sr.passwordHandler = &security.Argon2PasswordHandler{
		EncryptionParameters: sr.parameters.EncryptionParameters,
	}
}

func (sr *ServiceResources) setUpAuthorizationResource(controllerRegistry *controllers.ControllerRegistry) {
	service := authorization.NewAuthorizationService(
		sr.repository,
		sr.passwordHandler,
		sr.tokenHandler,
		sr.parameters,
	)
	controllerRegistry.AddController(authorization.NewController(service))
}

func (sr ServiceResources) CleanUp() {
	sr.dataSource.Close()
}
